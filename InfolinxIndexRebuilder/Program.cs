﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.IO;


namespace InfolinxIndexRebuilder
{
    class Program
    {

        static void Main(string[] args)
        {
            string strSQL = String.Empty;
            List<string> lstSQL = new List<string>();
            bool StoredProc = true;

            if (args.Length > 0)
            {
                //this will be the path to the sql we want to run against the databases                
                try
                {
                    using (StreamReader streamReader = new StreamReader(args[0].ToString(), System.Text.Encoding.GetEncoding(1252)))
                    {
                        string line = String.Empty;

                        while ((line = streamReader.ReadLine()) != null)
                        {
                            if (line != "GO")
                            {
                                strSQL += line + Environment.NewLine;
                            }

                            if (line == "GO")
                            {
                                lstSQL.Add(strSQL);
                                strSQL = "";
                                StoredProc = false;
                            }
                        }

                        if (strSQL.Trim().Length > 0)
                        {
                            StoredProc = false;
                            lstSQL.Add(strSQL);
                        }
                    }

                }
                catch (Exception ex)
                {
                    WriteToEventLog($"Failed to open and read file {ex} {args[0]}", true);
                }
            }
            else
            {
                //lstSQL.Add("IX_REFILL_CONTENTS_TABLE");
                lstSQL.Add("IX_REBUILD_INDEXES");
            }

            WriteToEventLog($"IxRebuildIndexes started with {lstSQL.Count} SQL statement to run. {DateTime.Now}", false);

            string strLocalDB = @"DATA SOURCE=ILX-DEPLOYMENT\IXSQL2019;INITIAL CATALOG=AZURE_DATABASES;Integrated Security=SSPI;";

            DataTable objDt = new DataTable();
            objDt.Columns.Add("DATABASE_NAME", typeof(String));
            objDt.Columns.Add("DATABASE_REGION", typeof(String));
            objDt.Columns.Add("RUN_TIME", typeof(int));

            var selectSql = $"SELECT [DATABASE_NAME] ,[DATABASE_REGION], [RUN_TIME] FROM [AZURE_DATABASE_LIST] ORDER BY [DATABASE_REGION] DESC";

            //get list of dbs that we need to connect to 
            using (SqlConnection cn = new SqlConnection(strLocalDB))
            using (SqlCommand cmd = new SqlCommand(selectSql, cn))
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 1000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(objDt);
            }

            string CanadaConneciton = "DATA SOURCE=tcp:a0k7fridn2.database.windows.net,1433;INITIAL CATALOG=master;UID=timb;PWD=KYa21LfU89T2f8;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
            string GovtProdConnection = "DATA SOURCE=tcp:infolinxgovtsql.database.usgovcloudapi.net,1433;INITIAL CATALOG=master;UID=timb;PWD=KYa21LfU89T2f8;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
            string GovtDevConnection = "DATA SOURCE=tcp:infolinx-development.database.usgovcloudapi.net;INITIAL CATALOG=master;UID=infolinx-sa;PWD=Inf0l1nx!;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
            string USAProdConnection = "DATA SOURCE=tcp:ojp0lff8sg.database.windows.net;INITIAL CATALOG=master;UID=timb;PWD=KYa21LfU89T2f8;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
            string USADevConnection = "DATA SOURCE=tcp:infolinx-development.database.windows.net;INITIAL CATALOG=master;UID=infolinx-sa;PWD=Inf0l1nx!;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
            string NorthEurpoeConnection = "DATA SOURCE=tcp:infolinx-europe-north.database.windows.net;INITIAL CATALOG=master;UID=infolinx-sa;PWD=Inf0l1nx!;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";

            List<string> lstDatabases = new List<string> { CanadaConneciton, GovtProdConnection, GovtDevConnection, USAProdConnection, USADevConnection, NorthEurpoeConnection };

            foreach (string connection in lstDatabases)
            {
                var region = "US-PROD";
                switch (connection)
                {
                    case "DATA SOURCE=tcp:a0k7fridn2.database.windows.net,1433;INITIAL CATALOG=master;UID=timb;PWD=KYa21LfU89T2f8;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;":
                        region = "CA";
                        break;
                    case "DATA SOURCE=tcp:infolinxgovtsql.database.usgovcloudapi.net,1433;INITIAL CATALOG=master;UID=timb;PWD=KYa21LfU89T2f8;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;":
                        region = "GOV-PROD";
                        break;
                    case "DATA SOURCE=tcp:infolinx-development.database.usgovcloudapi.net;INITIAL CATALOG=master;UID=infolinx-sa;PWD=Inf0l1nx!;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;":
                        region = "GOV-DEV";
                        break;
                    case "DATA SOURCE=tcp:infolinx-development.database.windows.net;INITIAL CATALOG=master;UID=infolinx-sa;PWD=Inf0l1nx!;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;":
                        region = "US-DEV";
                        break;
                    case "DATA SOURCE=tcp:infolinx-europe-north.database.windows.net;INITIAL CATALOG=master;UID=infolinx-sa;PWD=Inf0l1nx!;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;":
                        region = "EU";
                        break;
                }

                CheckDatabaseEntries(objDt, connection, region);
            }

            List<Task> lstTasks = new List<Task>();
            DataView currentTimeDbs = objDt.DefaultView;
            currentTimeDbs.RowFilter = $"RUN_TIME={DateTime.Now.Hour}";

            string commaSeparatedString = String.Join(",", currentTimeDbs.ToTable().AsEnumerable().Select(x => x.Field<string>("DATABASE_NAME").ToString()).ToArray());

            commaSeparatedString = commaSeparatedString.Length > 0 ? commaSeparatedString : "No";

            WriteToEventLog($"{commaSeparatedString} databases to be updated started at {DateTime.Now.ToString()}", false);

            if (currentTimeDbs.Count > 0)
            {
                foreach (DataRowView drv in currentTimeDbs)
                {
                    DataRow row = drv.Row;

                    Task runTask = Task.Factory.StartNew(() => RebuildIndexes(row["DATABASE_NAME"].ToString(), row["DATABASE_REGION"].ToString(), lstSQL, StoredProc));

                    lstTasks.Add(runTask);
                }

                Task.WaitAll(lstTasks.ToArray());

                WriteToEventLog("IxRebuildIndexes finished " + DateTime.Now.ToString() + " on " + lstTasks.Count.ToString() + " databases.", false);
            }
        }

        private static void CheckDatabaseEntries(DataTable existingDt, string connectionstring, string region)
        {
            string strSQLStatement = "SELECT name, database_id, create_date FROM sys.databases where name NOT IN ('master','ASPState')";

            string strLocalDB = @"DATA SOURCE=ILX-DEPLOYMENT\IXSQL2019;INITIAL CATALOG=AZURE_DATABASES;Integrated Security=SSPI;";

            DataTable dt = new DataTable();

            try
            {
                using (var conn = new SqlConnection(connectionstring))
                using (var dataAdapter = new SqlDataAdapter(strSQLStatement, conn))
                {
                    dataAdapter.Fill(dt);
                }

                foreach (DataRow row in dt.Rows)
                {
                    bool blnFound = false;
                    foreach (DataRow existingRow in existingDt.Rows)
                    {
                        if (row["name"].ToString() == existingRow["DATABASE_NAME"].ToString())
                        {
                            blnFound = true;
                            break;
                        }
                    }
                    if (!blnFound)
                    {
                        InsertMissingDB(strLocalDB, row["name"].ToString(), connectionstring.Substring(0, connectionstring.IndexOf(";")));                        
                    }
                }

            }
            catch (Exception ex)
            {

            }

        }
        private static void InsertMissingDB(string connString, string name, string source)
        {
            string strSQLStatement = $"SELECT * FROM DBS_TO_ADD WHERE DATABASE_NAME='{name}' AND DATABASE_SOURCE='{source}'";
            DataTable dataTable = new DataTable();
            using (SqlConnection cn = new SqlConnection(connString))
            using (SqlCommand cmd = new SqlCommand(strSQLStatement, cn))
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 1000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dataTable);
            }

            if (dataTable.Rows.Count == 0)
            {
                strSQLStatement = $"INSERT INTO DBS_TO_ADD VALUES('{name}','{source}',12)";
                try
                {
                    using (var conn = new SqlConnection(connString))
                    using (var command = new SqlCommand(strSQLStatement, conn)
                    {

                        CommandTimeout = 100,
                        CommandType = CommandType.Text

                    })
                    {
                        conn.Open();
                        command.ExecuteNonQuery();
                    }

                }
                catch (Exception ex)
                {
                    WriteToEventLog($"Error writting to local db {ex}", true);
                }
            }
        }
        private static void WriteToEventLog(string message, bool err)
        {
            if (!EventLog.SourceExists("IxRebuildIndex"))
            {
                EventLog.CreateEventSource("IxRebuildIndex", "IxRebuildIndex");
            }
            if (err)
            {
                EventLog.WriteEntry("IxRebuildIndex", message, EventLogEntryType.Error);
            }
            else
            {
                EventLog.WriteEntry("IxRebuildIndex", message, EventLogEntryType.Information);
            }
        }

        static void RebuildIndexes(string DatabaseName, string Region, List<string> lstSQL, bool StoredProc)
        {
            string CanadaConneciton = "DATA SOURCE=tcp:a0k7fridn2.database.windows.net,1433;INITIAL CATALOG=DATABASENAME;UID=timb;PWD=KYa21LfU89T2f8;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";

            string GovtProdConnection = "DATA SOURCE=tcp:infolinxgovtsql.database.usgovcloudapi.net,1433;INITIAL CATALOG=DATABASENAME;UID=timb;PWD=KYa21LfU89T2f8;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
            string GovtDevConnection = "DATA SOURCE=tcp:infolinx-development.database.usgovcloudapi.net;INITIAL CATALOG=DATABASENAME;UID=infolinx-sa;PWD=Inf0l1nx!;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;"; ;

            string USAProdConnection = "DATA SOURCE=tcp:ojp0lff8sg.database.windows.net;INITIAL CATALOG=DATABASENAME;UID=timb;PWD=KYa21LfU89T2f8;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
            string USADevConnection = "DATA SOURCE=tcp:infolinx-development.database.windows.net;INITIAL CATALOG=DATABASENAME;UID=infolinx-sa;PWD=Inf0l1nx!;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";

            string NorthEurpoeConnection = "DATA SOURCE=tcp:infolinx-europe-north.database.windows.net;INITIAL CATALOG=DATABASENAME;UID=infolinx-sa;PWD=Inf0l1nx!;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;"; ;

            //US Government Virginia SQL Server infolinxgovtsql Production timb    KYa21LfU89T2f8 infolinxgovtsql.database.usgovcloudapi.net

            //US Government Virginia  SQL Server  infolinx - development    Development infolinx-sa Inf0l1nx!   infolinx - development.database.usgovcloudapi.net
            //East US SQL Server  infolinx - development    Development infolinx-sa Inf0l1nx!   infolinx - development.database.windows.net
            //East US SQL Server  ojp0lff8sg Production  timb KYa21LfU89T2f8  ojp0lff8sg.database.windows.net
            //Canada Central SQL Server a0k7fridn2  Production timb    KYa21LfU89T2f8 a0k7fridn2.database.windows.net
            //North Europe SQL Server infolinx-europe - north   Production infolinx-sa Inf0l1nx!   infolinx - europe - north.database.windows.net


            string ConnectionString = String.Empty;
            int commandTimeOut = 6000;

            List<string> lstLargeDBs = new List<string> { "TEXAS_33", "AMGEN_35", "BBVACOMPASS_35", "CNO_36","PHILLIP_MORRIS_PRODUCTS_37_TEST",
                                                          "PHILLIP_MORRIS_PRODUCTS_37_QA","PHILLIP_MORRIS_PRODUCTS_37_DEV","PHILLIP_MORRIS_PRODUCTS_37" };

            switch (Region)
            {
                case "US-PROD":
                    ConnectionString = USAProdConnection.Replace("DATABASENAME", DatabaseName);
                    break;
                case "US-DEV":
                    ConnectionString = USADevConnection.Replace("DATABASENAME", DatabaseName);
                    break;
                case "CA":
                    ConnectionString = CanadaConneciton.Replace("DATABASENAME", DatabaseName); ;
                    break;
                case "GOV-PROD":
                    ConnectionString = GovtProdConnection.Replace("DATABASENAME", DatabaseName); ;
                    break;
                case "GOV-DEV":
                    ConnectionString = GovtDevConnection.Replace("DATABASENAME", DatabaseName); ;
                    break;
                case "EU":
                    ConnectionString = NorthEurpoeConnection.Replace("DATABASENAME", DatabaseName); ;
                    break;
            }

            if (lstLargeDBs.Contains(DatabaseName))
            {
                commandTimeOut = 20000;
            }

            foreach (string strSQLStatement in lstSQL)
            {
                try
                {
                    using (var conn = new SqlConnection(ConnectionString))
                    using (var command = new SqlCommand(strSQLStatement, conn)
                    {

                        CommandTimeout = commandTimeOut,
                        CommandType = StoredProc == true ? CommandType.StoredProcedure : CommandType.Text

                    })
                    {
                        conn.Open();
                        command.ExecuteNonQuery();
                    }

                }
                catch (SqlException sqlEx)
                {
                    if (sqlEx.Number == -2) //timeout exception
                    {
                        WriteToEventLog("IxRebuildIndexes timeout error " + DateTime.Now.ToString() + sqlEx.ToString() + " on " + DatabaseName + ".", true);
                        try
                        {
                            using (var conn = new SqlConnection(ConnectionString))
                            using (var command = new SqlCommand(strSQLStatement, conn)
                            {
                                CommandTimeout = 20000,
                                CommandType = StoredProc == true ? CommandType.StoredProcedure : CommandType.Text

                            })
                            {
                                conn.Open();
                                command.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ix)
                        {
                            WriteToEventLog("IxRebuildIndexes error " + DateTime.Now.ToString() + ix.ToString() + " on " + DatabaseName + ".", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    WriteToEventLog("IxRebuildIndexes error " + DateTime.Now.ToString() + ex.ToString() + " on " + DatabaseName + ".", true);
                }
            }
        }
    }
}
